$(document).ready(function () {

//header
$('#js-open-menu').on('click', function() {
  $('.header__nav').toggleClass('header__nav--show');
});

// admin form
  function hideForm() {
    $('.form__content').animate({
      opacity: 0
    }, 500);
    setTimeout(function(){
      $('.form__content').css('display', 'none');
    }, 500);
  }

  function showForm() {
    $('.form__content').animate({
      opacity: 1
    }, 500);
    $('.form__content').css('display', 'block');
  }
  $(document).on('click', '.form__title--icon.fa-plus', function () {
    showForm();
    $(this).attr('class', 'form__title--icon fa fa-minus');
  });
  $(document).on('click', '.form__title--icon.fa-minus', function () {
    hideForm();
    $(this).attr('class', 'form__title--icon fa fa-plus');
  });

  // modal
  $('.js-close-notification').on('click', function() {
    hideNotification();
  });
  $('.js-remove-group').on('click', function() {
    showNotification('.notification--delete-group');
  });
  $('.js-add-disciplines-time').on('click', function() {
    showNotification('.notification--time-disciplines');
  });
  $('.js-add-teacher').on('click', function() {
    showNotification('.notification--teacher');
  });
  $('.notification__btn--cancel').on('click', function() {
    hideNotification();
  });

  function hideNotification() {
    $('.notification').animate({
      top: "-120px",
      opacity: 0
    }, 500);
    $('.notification__back').css('display', 'none');
    $('body').css('overflow', 'auto');
  }

  function showNotification(specificNotification) {
    $(specificNotification).animate({
      top: "100px",
      opacity: 1
    }, 500);
    $('.notification__back').css('display', 'block');
    $('body').css('overflow', 'hidden');
  }

  $('#js-open-menu').on('click', function() {
    $('.header__nav').toggleClass('header__nav--show');
  });

  //function(specificNotification) {}
});
